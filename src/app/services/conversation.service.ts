import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class ConversationService {
  constructor(private angularFireDatabase: AngularFireDatabase) {}
  public createConversation(conversation) {
    return this.angularFireDatabase
      .object(
        'conversations/' + conversation.uid + '/' + conversation.timestamp
      )
      .set(conversation);
  }
  public getConversation(uid) {
    return this.angularFireDatabase.list('conversations/' + uid);
  }
  public editConversation(conversation) {
    return this.angularFireDatabase
      .object(
        'conversations/' + conversation.uid + '/' + conversation.timestamp
      )
      .set(conversation);
  }
}
