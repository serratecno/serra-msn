import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '@app/interfaces/user';
import { UserService } from '@app/services/user.service';
import { ConversationService } from '@app/services/conversation.service';
import { AuthenticationService } from '@app/services/authentication.service';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.scss']
})
export class ConversationComponent implements OnInit {
  public friendId: any;
  public friend: User;
  public user: User;
  public conversation_id: string;
  public textMessage: string;
  public conversation: any[];
  public shake = false;
  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private conversationService: ConversationService,
    private authenticationService: AuthenticationService
  ) {
    this.friendId = this.activatedRoute.snapshot.params['uid'];
    console.log(this.friendId);
    this.authenticationService.getStatus().subscribe(session => {
      this.userService
        .getUserById(session.uid)
        .valueChanges()
        .subscribe((user: User) => {
          this.user = user;
          this.userService
            .getUserById(this.friendId)
            .valueChanges()
            .subscribe(
              (data: User) => {
                this.friend = data;
                const ids = [this.user.uid, this.friend.uid].sort();
                this.conversation_id = ids.join('|');
                this.getConversation();
              },
              error => {
                console.log(error);
              }
            );
        });
    });
  }

  public ngOnInit() {}
  public sendMessage() {
    const message = {
      uid: this.conversation_id,
      timestamp: Date.now(),
      text: this.textMessage,
      sender: this.user.uid,
      receiver: this.friend.uid,
      type: 'text'
    };
    this.conversationService.createConversation(message).then(() => {
      this.textMessage = '';
    });
  }
  public sendZumbido() {
    const message = {
      uid: this.conversation_id,
      timestamp: Date.now(),
      text: null,
      sender: this.user.uid,
      receiver: this.friend.uid,
      type: 'zumbido'
    };
    this.conversationService.createConversation(message).then(() => {});
    this.doZumbido();
  }
  public doZumbido() {
    const audio = new Audio('assets/sound/zumbido.m4a');
    audio.play();
    this.shake = true;
    window.setTimeout(() => {
      this.shake = false;
    }, 1000);
  }
  public getConversation() {
    this.conversationService
      .getConversation(this.conversation_id)
      .valueChanges()
      .subscribe(
        data => {
          this.conversation = data;
          this.conversation.forEach(message => {
            if (!message.seen) {
              message.seen = true;
              this.conversationService.editConversation(message);
              if (message.type === 'text') {
                const audio = new Audio('assets/sound/new_message.m4a');
                audio.play();
              } else if (message.type === 'zumbido') {
                this.doZumbido();
              }
            }
          });
          console.log(data);
        },
        error => {
          console.log(error);
        }
      );
  }
  public getUserNickById(id) {
    if (id === this.friend.uid) {
      return this.friend.nick;
    } else {
      return this.user.nick;
    }
  }
}
