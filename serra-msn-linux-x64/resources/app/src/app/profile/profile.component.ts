import { Component, OnInit } from '@angular/core';
import { User } from '@app/interfaces/user';
import { UserService } from '@app/services/user.service';
import { AuthenticationService } from '@app/services/authentication.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public user: User;
  public imageChangedEvent: any = '';
  public croppedImage: any = '';
  public picture: any;

  constructor(
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private firebaseStorage: AngularFireStorage
  ) {
    this.authenticationService.getStatus().subscribe(
      status => {
        this.userService
          .getUserById(status.uid)
          .valueChanges()
          .subscribe(
            (data: User) => {
              this.user = data;
              console.log(this.user);
            },
            error => {
              console.log(error);
            }
          );
      },
      error => {
        console.log(error);
      }
    );
  }

  public ngOnInit() {}
  public saveSettings() {
    if (this.croppedImage) {
      const currentPictureId = Date.now();
      const pictures = this.firebaseStorage
        .ref('pictures/' + currentPictureId + '.jpg')
        .putString(this.croppedImage, 'data_url');
      pictures
        .then(result => {
          this.picture = this.firebaseStorage
            .ref('pictures/' + currentPictureId + '.jpg')
            .getDownloadURL();
          this.picture.subscribe(p => {
            this.userService
              .setAvatar(p, this.user.uid)
              .then(() => {
                alert('Avatar subido correctamentne');
              })
              .catch(error => {
                alert('Hubo un error al tratar de subir la imagen');
                console.log(error);
              });
          });
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      this.userService
        .editUser(this.user)
        .then(() => {
          alert('Cambios guardados!');
        })
        .catch(error => {
          alert('Hubo un error');
          console.log(error);
        });
    }
  }
  public fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  public imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  public imageLoaded() {
    // show cropper
  }
  public cropperReady() {
    // cropper ready
  }
  public loadImageFailed() {
    // show message
  }
}
