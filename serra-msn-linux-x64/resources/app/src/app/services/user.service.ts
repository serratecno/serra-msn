import { Injectable } from '@angular/core';
import { User } from '@app/interfaces/user';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public friends: User[];

  constructor(private angularFireDatabase: AngularFireDatabase) {}

  public getUsers() {
    return this.angularFireDatabase.list('/users');
  }
  public getUserById(uid) {
    return this.angularFireDatabase.object('/users/' + uid);
  }
  public createUser(user) {
    return this.angularFireDatabase.object('/users/' + user.uid).set(user);
  }
  public editUser(user) {
    return this.angularFireDatabase.object('/users/' + user.uid).set(user);
  }
  public setAvatar(avatar, uid) {
    return this.angularFireDatabase
      .object('/users/' + uid + '/avatar')
      .set(avatar);
  }
  public addFriend(userId, friendId) {
    this.angularFireDatabase
      .object('users/' + userId + '/friends/' + friendId)
      .set(friendId);
    return this.angularFireDatabase
      .object('users/' + friendId + '/friends/' + userId)
      .set(userId);
  }
}
