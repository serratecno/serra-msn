import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {
  constructor(private angularFireDatabase: AngularFireDatabase) {}
  public createRequest(request) {
    const cleanEmail = request.receiver_email.replace('.', ',');
    return this.angularFireDatabase
      .object('requests/' + cleanEmail + '/' + request.sender)
      .set(request);
  }
  // enviar status  aceptado, reachazado o pendiente
  public setRequestStatus(request, status) {
    const cleanEmail = request.receiver_email.replace('.', ',');
    return this.angularFireDatabase
      .object('requests/' + cleanEmail + '/' + request.sender + '/status')
      .set(status);
  }
  public getRequestsForEmail(email) {
    const cleanEmail = email.replace('.', ',');
    return this.angularFireDatabase.list('requests/' + cleanEmail);
  }
}
