import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@app/services/authentication.service';
import { UserService } from '@app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public operation = 'login';
  public email: string = null;
  public password: string = null;
  public nick: string = null;
  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private router: Router
  ) {}

  public ngOnInit() {}

  public login() {
    this.authenticationService
      .loginWithEmail(this.email, this.password)
      .then(data => {
        alert('Loggeado correctamente');
        console.log(data);
        this.router.navigate(['home']);
      })
      .catch(error => {
        alert('Ocurrioo un error');
        console.log(error);
      });
  }

  public register() {
    this.authenticationService
      .registerWithEmail(this.email, this.password)
      .then(data => {
        const user = {
          uid: data.user.uid,
          email: this.email,
          nick: this.nick
        };
        this.userService
          .createUser(user)
          .then(data2 => {
            alert('Registrado correctamente');
            console.log(data2);
          })
          .catch(error => {
            alert('Ocurrioo un error');
            console.log(error);
          });
      })
      .catch(error => {
        alert('Ocurrioo un error');
        console.log(error);
      });
  }
}
