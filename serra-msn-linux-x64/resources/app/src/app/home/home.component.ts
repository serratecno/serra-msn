import { Component, OnInit } from '@angular/core';
import { User } from '../interfaces/user';
import { UserService } from '@app/services/user.service';
import { AuthenticationService } from '@app/services/authentication.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RequestsService } from '@app/services/requests.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public friends: User[];
  public query = '';
  public friendEmail = '';
  public user: User;
  constructor(
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private modalService: NgbModal,
    private requestsService: RequestsService
  ) {
    this.userService
      .getUsers()
      .valueChanges()
      .subscribe(
        (data: User[]) => {
          this.friends = data;
        },
        error => {
          console.log(error);
        }
      );
    this.authenticationService.getStatus().subscribe(status => {
      this.userService
        .getUserById(status.uid)
        .valueChanges()
        .subscribe((data: User) => {
          this.user = data;
          if (this.user.friends) {
            this.user.friends = Object.values(this.user.friends);
            console.log(this.user);
          }
        });
    });
  }

  public ngOnInit() {}
  public logout() {
    this.authenticationService
      .logOut()
      .then(() => {
        alert('Sesioon Cerrada');
        this.router.navigate(['login']);
      })
      .catch(error => {
        console.log(error);
      });
  }
  public open(content) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(result => {}, reason => {});
  }
  public sendRequest() {
    const request = {
      timestamp: Date.now(),
      receiver_email: this.friendEmail,
      sender: this.user.uid,
      status: 'pending'
    };
    this.requestsService
      .createRequest(request)
      .then(() => {
        alert('Solicitud Enviada');
      })
      .catch(error => {
        alert('Hubo un error');
        console.log(error);
      });
  }
}
